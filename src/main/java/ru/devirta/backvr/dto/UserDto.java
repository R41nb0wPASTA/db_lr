package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.Role;
import ru.devirta.backvr.model.User;

import java.util.List;

@NoArgsConstructor
@ToString
@Data
public class UserDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @NotNull
    private Long roleId;

    //@NotNull
    //private List<Form> forms;

    public UserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.roleId = user.getRoleId();
        //this.forms = user.getForms();
    }

}