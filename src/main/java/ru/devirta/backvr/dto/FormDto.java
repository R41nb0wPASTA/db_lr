package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.User;

@NoArgsConstructor
@ToString
@Data
public class FormDto {
    @NotNull
    private Long id;

    @NotNull
    private Boolean priv;

    @NotNull
    private Long user_id;

    @NotNull
    private Long avatar_id;

    @NotNull
    private User user;

    public FormDto(Form form) {
        this.id = form.getId();
        this.priv = form.getPriv();
        this.user_id = form.getUser_id();
        this.avatar_id = form.getAvatar_id();
        //this.user = form.getUser();
    }
}