package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Session;

@NoArgsConstructor
@ToString
@Data
public class SessionDto {
    @NotNull
    private Long id;

    @NotNull
    private Long user_id;

    public SessionDto(Session session) {
        this.id = session.getId();
        this.user_id = session.getUser_id();
    }
}
