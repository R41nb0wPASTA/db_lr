package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Avatar;

@NoArgsConstructor
@ToString
@Data
public class AvatarDto {
    @NotNull
    private Long id;

    @NotNull
    private String link;

    @NotNull
    private Long user_id;

    public AvatarDto(Avatar avatar) {
        this.id = avatar.getId();
        this.link = avatar.getLink();
        this.user_id = avatar.getUser_id();
    }
}
