package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.devirta.backvr.model.Role;

@NoArgsConstructor
@ToString
@Data
public class RoleDto {
    @NotNull
    private Long id;

    @NotNull
    private String roleName;

    @Nullable
    private String description;

    public RoleDto(Role role) {
        this.id = role.getId();
        this.roleName = role.getRoleName();
        this.description = role.getDescription();
    }
}
