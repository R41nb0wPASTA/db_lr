package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.model.VariantOfAnswer;

@NoArgsConstructor
@ToString
@Data
public class SessionResultDto {
    @NotNull
    private Long id;

    @NotNull
    private Long session_id;

    @NotNull
    private Long variant_of_answer_id;

    @NotNull
    private VariantOfAnswer variantOfAnswer;

    public SessionResultDto(SessionResult sessionResult) {
        this.id = sessionResult.getId();
        this.session_id = sessionResult.getSession_id();
        this.variant_of_answer_id = sessionResult.getVariant_of_answer_id();
    }
}
