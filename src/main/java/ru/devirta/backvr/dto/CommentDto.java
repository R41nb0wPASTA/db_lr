package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Comment;
import ru.devirta.backvr.model.User;

@NoArgsConstructor
@ToString
@Data
public class CommentDto {
    @NotNull
    private Long id;

    @NotNull
    private String text;

    @NotNull
    private Long user_id;

    @NotNull
    private Long question_id;

    @NotNull
    private User user;

    public CommentDto(Comment comment) {
        this.id = comment.getId();
        this.text = comment.getText();
        this.user_id = comment.getUser_id();
        this.question_id = comment.getQuestion_id();
    }
}
