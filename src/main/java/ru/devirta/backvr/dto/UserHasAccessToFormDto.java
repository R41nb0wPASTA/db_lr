package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.UserHasAccessToForm;

@NoArgsConstructor
@ToString
@Data
public class UserHasAccessToFormDto {
    @NotNull
    private Long user_id;

    @NotNull
    private Long form_id;

    public UserHasAccessToFormDto(UserHasAccessToForm userHasAccessToForm) {
        this.user_id = userHasAccessToForm.getUser_id();
        this.form_id = userHasAccessToForm.getForm_id();
    }
}
