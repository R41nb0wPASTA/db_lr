package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.VariantOfAnswer;

@NoArgsConstructor
@ToString
@Data
public class VariantOfAnswerDto {
    @NotNull
    private Long id;

    @Nullable
    private String contents;

    @NotNull
    private Long question_id;

    @NotNull
    private Question question;

    public VariantOfAnswerDto(VariantOfAnswer variantOfAnswerDto) {
        this.id = variantOfAnswerDto.getId();
        this.contents = variantOfAnswerDto.getContents();
        this.question_id = variantOfAnswerDto.getQuestion_id();
    }
}
