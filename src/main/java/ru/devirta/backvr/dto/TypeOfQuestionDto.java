package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.TypeOfQuestion;

@NoArgsConstructor
@ToString
@Data
public class TypeOfQuestionDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;

    public TypeOfQuestionDto(TypeOfQuestion typeOfQuestion) {
        this.id = typeOfQuestion.getId();
        this.name = typeOfQuestion.getName();
    }
}
