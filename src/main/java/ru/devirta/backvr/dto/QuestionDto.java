package ru.devirta.backvr.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.VariantOfAnswer;

import java.util.List;

@NoArgsConstructor
@ToString
@Data
public class QuestionDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Boolean optional;

    @NotNull
    private Long type_of_question_id;

    @NotNull
    private Long form_id;

    @NotNull
    private Form form;

    public QuestionDto(Question question) {
        this.id = question.getId();
        this.name = question.getName();
        this.optional = question.getOptional();
        this.type_of_question_id = question.getType_of_question_id();
        this.form_id = question.getForm_id();
    }
}
