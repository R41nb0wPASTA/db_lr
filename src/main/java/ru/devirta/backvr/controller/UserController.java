package ru.devirta.backvr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.UserDto;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.service.UserService;

@RestController()
@RequestMapping(value = "/api/users/")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    //@Secured(value = {"ROLE_ADMIN", "ROLE_MOD"})
    @GetMapping("/getByEmail/{email}")
    @ApiOperation(value = "Возвращает пользователя по эл. почте")
    public ResponseEntity<UserDto> getUserByEmail(@PathVariable(value = "email") String email) {
        try {
            return new ResponseEntity<>(userService.getUserByEmail(email), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает пользователя по id")
    public ResponseEntity<UserDto> getUserById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/registration")
    @ApiOperation(value = "Регистрирует нового пользователя")
    public ResponseEntity<UserDto> save(@RequestBody UserDto userDto) {
        return new ResponseEntity<>(userService.saveUser(userDto), HttpStatus.CREATED);
    }

    @GetMapping("/login")
    public ResponseEntity<UserDto> getAuthUserCredentials() {
        try {
            return new ResponseEntity<>(userService.getAuthUserCredentials(), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    //@Secured(value = {"ROLE_ADMIN", "ROLE_MOD"})
    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "Удаляет пользователя")
    public ResponseEntity delete() {
        try{
            userService.delete(userService.getAuthUserCredentials().getId());
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (UserNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}