package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.AvatarDto;
import ru.devirta.backvr.exception.AvatarNotFoundException;
import ru.devirta.backvr.exception.RoleNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.Avatar;
import ru.devirta.backvr.service.AvatarService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/avatars/")
public class AvatarController {
    private final AvatarService avatarService;

    @Autowired
    public AvatarController(AvatarService avatarService) {
        this.avatarService = avatarService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает аватар по id")
    public ResponseEntity<AvatarDto> getRoleById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(avatarService.getAvatarById(id), HttpStatus.OK);
        } catch (AvatarNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый аватар")
    public ResponseEntity<AvatarDto> save(@RequestBody AvatarDto avatarDto) {
        return new ResponseEntity<>(avatarService.saveAvatar(avatarDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет аватар")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            avatarService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (AvatarNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
