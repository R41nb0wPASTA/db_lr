package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.RoleDto;
import ru.devirta.backvr.dto.UserDto;
import ru.devirta.backvr.exception.RoleNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.service.RoleService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/roles/")
public class RoleController {
    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает роль по id")
    public ResponseEntity<RoleDto> getRoleById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(roleService.getRoleById(id), HttpStatus.OK);
        } catch (RoleNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByName/{name}")
    @ApiOperation(value = "Возвращает роль по имени")
    public ResponseEntity<RoleDto> getRoleByName(@PathVariable(value = "name") String name) {
        try {
            return new ResponseEntity<>(roleService.getRoleByName(name), HttpStatus.OK);
        } catch (RoleNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новую роль")
    public ResponseEntity<RoleDto> save(@RequestBody RoleDto roleDto) {
        return new ResponseEntity<>(roleService.saveRole(roleDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет роль")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            roleService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (RoleNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
