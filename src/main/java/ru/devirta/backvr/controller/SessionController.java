package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.SessionDto;
import ru.devirta.backvr.exception.SessionNotFoundException;
import ru.devirta.backvr.service.SessionService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/sessions/")
public class SessionController {
    private final SessionService sessionService;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает сессию по id")
    public ResponseEntity<SessionDto> getSessionById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(sessionService.getSessionById(id), HttpStatus.OK);
        } catch (SessionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новую сессию")
    public ResponseEntity<SessionDto> save(@RequestBody SessionDto sessionDto) {
        return new ResponseEntity<>(sessionService.saveSession(sessionDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет сессию")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            sessionService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (SessionNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
