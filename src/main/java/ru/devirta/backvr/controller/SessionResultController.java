package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.SessionResultDto;
import ru.devirta.backvr.exception.SessionResultNotFoundException;
import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.service.SessionResultService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/sessionResults/")
public class SessionResultController {
    private final SessionResultService sessionResultService;

    @Autowired
    public SessionResultController(SessionResultService sessionResultService) {
        this.sessionResultService = sessionResultService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает результат сессии по id")
    public ResponseEntity<SessionResultDto> getSessionResultById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(sessionResultService.getSessionResultById(id), HttpStatus.OK);
        } catch (SessionResultNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByQuestionId/{id}")
    @ApiOperation(value = "Возвращает результат сессии по id вопроса")
    public ResponseEntity <List<SessionResult>> getSessionResultByQuestionId(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(sessionResultService.getSessionResultByQuestionId(id), HttpStatus.OK);
        } catch (SessionResultNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByFormId/{id}")
    @ApiOperation(value = "Возвращает результат сессии по id формы")
    public ResponseEntity <List<SessionResult>> getSessionResultByFormId(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(sessionResultService.getSessionResultByFormId(id), HttpStatus.OK);
        } catch (SessionResultNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый результат сессии")
    public ResponseEntity<SessionResultDto> save(@RequestBody SessionResultDto sessionResultDto) {
        return new ResponseEntity<>(sessionResultService.saveSessionResult(sessionResultDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет результат сессии")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            sessionResultService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (SessionResultNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
