package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.FormDto;
import ru.devirta.backvr.exception.FormNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.VariantOfAnswer;
import ru.devirta.backvr.service.FormService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/forms/")
public class FormController {
    private final FormService formService;

    @Autowired
    public FormController(FormService formService) {
        this.formService = formService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает форму по id")
    public ResponseEntity<FormDto> getFormById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(formService.getFormById(id), HttpStatus.OK);
        } catch (FormNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByUserId/{userId}")
    @ApiOperation(value = "Возвращает все формы пользователя по id")
    public ResponseEntity<List<Form>> getAllFormsByUserID(@PathVariable(value = "userId") Long userId) {
        try {
            return new ResponseEntity<>(formService.getAllFormsByUserId(userId), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getAllQuestionByFormId/{formId}")
    @ApiOperation(value = "Возвращает все вопросы формы по id")
    public ResponseEntity<List<Question>> getAllQuestionsByFormID(@PathVariable(value = "formId") Long formId) {
        try {
            return new ResponseEntity<>(formService.getFormQuestionsWithVarsById(formId), HttpStatus.OK);
        } catch (FormNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getAllVarsByQuestionId/{qId}")
    @ApiOperation(value = "Возвращает все ответы на вопрос по id")
    public ResponseEntity<List<VariantOfAnswer>> getAllVatsByQuestionID(@PathVariable(value = "qId") Long qId) {
        try {
            return new ResponseEntity<>(formService.getAllVarsByQuestionId(qId), HttpStatus.OK);
        } catch (FormNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /*@GetMapping("/getMostCommon")
    @ApiOperation(value = "Возвращает самую популярную форму")
    public ResponseEntity<Form> getMostPopularForm() {
        try {
            return new ResponseEntity<>(formService.getMostPopularForm(), HttpStatus.OK);
        } catch (FormNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }*/

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новую форму")
    public ResponseEntity<FormDto> save(@RequestBody FormDto formDto) {
        return new ResponseEntity<>(formService.saveForm(formDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет форму")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            formService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (FormNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
