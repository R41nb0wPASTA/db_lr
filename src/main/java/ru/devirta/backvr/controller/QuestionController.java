package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.QuestionDto;
import ru.devirta.backvr.exception.QuestionNotFoundException;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.service.QuestionService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/questions/")
public class QuestionController {
    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает вопрос по id")
    public ResponseEntity<QuestionDto> getQuestionById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(questionService.getQuestionById(id), HttpStatus.OK);
        } catch (QuestionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый вопрос")
    public ResponseEntity<QuestionDto> save(@RequestBody QuestionDto questionDto) {
        return new ResponseEntity<>(questionService.saveQuestion(questionDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет вопрос")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            questionService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (QuestionNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
