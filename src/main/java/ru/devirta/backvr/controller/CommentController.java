package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.CommentDto;
import ru.devirta.backvr.exception.CommentNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.Comment;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.service.CommentService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/comments/")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает комментарий по id")
    public ResponseEntity<CommentDto> getCommentById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(commentService.getCommentById(id), HttpStatus.OK);
        } catch (CommentNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByUserId/{userId}")
    @ApiOperation(value = "Возвращает все комментарии пользователя по id")
    public ResponseEntity<List<Comment>> getAllCommentsByUserID(@PathVariable(value = "userId") Long userId) {
        try {
            return new ResponseEntity<>(commentService.getAllCommentsByUserId(userId), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый комментарий")
    public ResponseEntity<CommentDto> save(@RequestBody CommentDto commentDto) {
        return new ResponseEntity<>(commentService.saveComment(commentDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет комментарий")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            commentService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (CommentNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
