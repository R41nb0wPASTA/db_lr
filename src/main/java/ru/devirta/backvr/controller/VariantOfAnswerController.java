package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.VariantOfAnswerDto;
import ru.devirta.backvr.exception.VariantOfAnswerNotFoundException;
import ru.devirta.backvr.model.VariantOfAnswer;
import ru.devirta.backvr.service.VariantOfAnswerService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/variantsOfAnswer/")
public class VariantOfAnswerController {
    private final VariantOfAnswerService variantOfAnswerService;

    @Autowired
    public VariantOfAnswerController(VariantOfAnswerService variantOfAnswerService) {
        this.variantOfAnswerService = variantOfAnswerService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает вариант ответа по id")
    public ResponseEntity<VariantOfAnswerDto> getVariantOfAnswerById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(variantOfAnswerService.getVariantOfAnswerById(id), HttpStatus.OK);
        } catch (VariantOfAnswerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый вариант ответа")
    public ResponseEntity<VariantOfAnswerDto> save(@RequestBody VariantOfAnswerDto variantOfAnswerDto) {
        return new ResponseEntity<>(variantOfAnswerService.saveVariantOfAnswer(variantOfAnswerDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет вариант ответа")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            variantOfAnswerService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (VariantOfAnswerNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
