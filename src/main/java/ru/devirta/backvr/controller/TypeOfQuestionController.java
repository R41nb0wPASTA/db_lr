package ru.devirta.backvr.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.devirta.backvr.dto.TypeOfQuestionDto;
import ru.devirta.backvr.exception.TypeOfQuestionNotFoundException;
import ru.devirta.backvr.service.TypeOfQuestionService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/typesOfQuestion/")
public class TypeOfQuestionController {
    private final TypeOfQuestionService typeOfQuestionService;

    @Autowired
    public TypeOfQuestionController(TypeOfQuestionService typeOfQuestionService) {
        this.typeOfQuestionService = typeOfQuestionService;
    }

    //@Secured(value = {"ROLE_ADMIN"})
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает тип вопроса по id")
    public ResponseEntity<TypeOfQuestionDto> getTypeOfQuestionById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(typeOfQuestionService.getTypeOfQuestionById(id), HttpStatus.OK);
        } catch (TypeOfQuestionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByName/{name}")
    @ApiOperation(value = "Возвращает тип вопроса по имени")
    public ResponseEntity<TypeOfQuestionDto> getTypeOfQuestionByName(@PathVariable(value = "name") String name) {
        try {
            return new ResponseEntity<>(typeOfQuestionService.getTypeOfQuestionByName(name), HttpStatus.OK);
        } catch (TypeOfQuestionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "Добавляет новый тип вопроса")
    public ResponseEntity<TypeOfQuestionDto> save(@RequestBody TypeOfQuestionDto typeOfQuestionDto) {
        return new ResponseEntity<>(typeOfQuestionService.saveTypeOfQuestion(typeOfQuestionDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "Удаляет тип вопроса")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            typeOfQuestionService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (TypeOfQuestionNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

