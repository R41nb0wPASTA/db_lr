package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import ru.devirta.backvr.dto.CommentDto;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "COMMENT")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "USER_ID")
    private Long user_id;

    @Column(name = "QUESTION_ID")
    private Long question_id;

    @ManyToOne()
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private User user;

    public Comment(CommentDto commentDto){
        this.id = commentDto.getId();
        this.text = commentDto.getText();
        this.user_id = commentDto.getUser_id();
        this.question_id = commentDto.getQuestion_id();
    }
}
