package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.SessionDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

import ru.devirta.backvr.dto.SessionResultDto;

@Entity
@Data
@NoArgsConstructor
@Table(name = "SESSION_RESULT")
public class SessionResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "SESSION_ID")
    private Long session_id;

    @Column(name = "VARIANT_OF_ANSWER_ID")
    private Long variant_of_answer_id;

    @ManyToOne()
    @JoinColumn(name = "VARIANT_OF_ANSWER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private VariantOfAnswer variantOfAnswer;

    public SessionResult(SessionResultDto sessionResultDto){
        this.id = sessionResultDto.getId();
        this.session_id = sessionResultDto.getSession_id();
        this.variant_of_answer_id = sessionResultDto.getVariant_of_answer_id();
    }
}
