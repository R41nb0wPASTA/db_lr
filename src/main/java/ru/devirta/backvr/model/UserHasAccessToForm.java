package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.SessionDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

import ru.devirta.backvr.dto.UserHasAccessToFormDto;

@Entity
@Data
@NoArgsConstructor
@Table(name = "USER_HAS_ACCESS_TO_FORM")
public class UserHasAccessToForm {
    @Id
    @Column(name = "USER_ID")
    private Long user_id;

    @Column(name = "FORM_ID")
    private Long form_id;

    public UserHasAccessToForm(UserHasAccessToFormDto userHasAccessToFormDto){
        this.user_id = userHasAccessToFormDto.getUser_id();
        this.form_id = userHasAccessToFormDto.getForm_id();
    }
}