package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.SessionDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

import ru.devirta.backvr.dto.TypeOfQuestionDto;

@Entity
@Data
@NoArgsConstructor
@Table(name = "TYPE_OF_QUESTION")
public class TypeOfQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public TypeOfQuestion(TypeOfQuestionDto typeOfQuestionDto){
        this.id = typeOfQuestionDto.getId();
        this.name = typeOfQuestionDto.getName();
    }
}
