package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.RoleDto;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "ROLE")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String roleName;

    @Column(name = "DESCRIPTION")
    private String description;

    public Role(RoleDto roleDto){
        this.id = roleDto.getId();
        this.roleName = roleDto.getRoleName();
        this.description = roleDto.getDescription();
    }
}
