package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.SessionDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

import ru.devirta.backvr.dto.SessionDto;

@Entity
@Data
@NoArgsConstructor
@Table(name = "SESSION")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_ID")
    private Long user_id;

    public Session(SessionDto sessionDto){
        this.id = sessionDto.getId();
        this.user_id = sessionDto.getUser_id();
    }
}
