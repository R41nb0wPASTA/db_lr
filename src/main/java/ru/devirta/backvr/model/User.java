package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.UserDto;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "USER", schema = "lr4", uniqueConstraints = @UniqueConstraint(columnNames = "EMAIL"))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ROLE_ID", nullable = false)
    private Long roleId;

    //@OneToMany()
    //@JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    //private List<Form> forms;


    /*@ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(
            name = "ROLE",
            joinColumns = @JoinColumn(name = "ID", referencedColumnName = "ROLE_ID")
    )*/
    //@ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "role_id")
    //private Role role;
   //@ManyToOne(fetch = FetchType.LAZY)
   //@JoinColumn(name = "role_id")
   //private Long roleId;


    public User(UserDto userDto){
        this.id = userDto.getId();
        this.name = userDto.getName();
        this.email = userDto.getEmail();
        this.password = userDto.getPassword();
        this.roleId = userDto.getRoleId();
        //this.forms = userDto.getForms();
    }

}
