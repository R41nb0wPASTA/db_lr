package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.AvatarDto;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "AVATAR")
public class Avatar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "LINK")
    private String link;

    @Column(name = "USER_ID")
    private Long user_id;

    public Avatar(AvatarDto avatarDto){
        this.id = avatarDto.getId();
        this.link = avatarDto.getLink();
        this.user_id = avatarDto.getUser_id();
    }
}
