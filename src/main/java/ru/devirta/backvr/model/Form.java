package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import ru.devirta.backvr.dto.FormDto;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "FORM")
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PRIVATE")
    private Boolean priv;

    @Column(name = "USER_ID")
    private Long user_id;

    @Column(name = "AVATAR_ID")
    private Long avatar_id;

    @ManyToOne()
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private User user;

    public Form(FormDto formDto){
        this.id = formDto.getId();
        this.priv = formDto.getPriv();
        this.user_id = formDto.getUser_id();
        this.avatar_id = formDto.getAvatar_id();
        //this.user = formDto.getUser();
    }

    public FormDto toDto() {
        FormDto formDto = new FormDto();
        formDto.setId(id);
        formDto.setPriv(priv);
        formDto.setUser_id(user_id);
        formDto.setAvatar_id(avatar_id);
        if (user != null) { formDto.setUser(user); }
        return formDto;
    }

}
