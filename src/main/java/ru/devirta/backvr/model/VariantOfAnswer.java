package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.devirta.backvr.dto.QuestionDto;
import ru.devirta.backvr.dto.SessionDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

import ru.devirta.backvr.dto.VariantOfAnswerDto;

@Entity
@Data
@NoArgsConstructor
@Table(name = "VARIANT_OF_ANSWER")
public class VariantOfAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CONTENTS")
    private String contents;

    @Column(name = "QUESTION_ID", insertable = false, updatable = false)
    private Long question_id;

    @ManyToOne()
    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private Question question;

    public VariantOfAnswer(VariantOfAnswerDto variantOfAnswerDto){
        this.id = variantOfAnswerDto.getId();
        this.contents = variantOfAnswerDto.getContents();
        this.question_id = variantOfAnswerDto.getQuestion_id();
    }

    public VariantOfAnswerDto toDto(VariantOfAnswerDto variantOfAnswerDto){
        this.id = variantOfAnswerDto.getId();
        this.contents = variantOfAnswerDto.getContents();

        return variantOfAnswerDto;
    }
}
