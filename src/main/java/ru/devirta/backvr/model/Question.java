package ru.devirta.backvr.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import ru.devirta.backvr.dto.QuestionDto;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "QUESTION")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "OPTIONAL")
    private Boolean optional;

    @Column(name = "TYPE_OF_QUESTION_ID")
    private Long type_of_question_id;

    @Column(name = "FORM_ID")
    private Long form_id;

    @ManyToOne()
    @JoinColumn(name = "FORM_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private Form form;

    public Question(QuestionDto questionDto){
        this.id = questionDto.getId();
        this.name = questionDto.getName();
        this.optional = questionDto.getOptional();
        this.type_of_question_id = questionDto.getType_of_question_id();
        this.form_id = questionDto.getForm_id();
    }

    public QuestionDto toDto(QuestionDto questionDto){
        this.id = questionDto.getId();
        this.name = questionDto.getName();

        return questionDto;
    }
}
