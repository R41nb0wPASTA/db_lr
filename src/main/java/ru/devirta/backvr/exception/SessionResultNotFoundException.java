package ru.devirta.backvr.exception;

public class SessionResultNotFoundException extends RuntimeException {
    public SessionResultNotFoundException(String message) {
        super(message);
    }
}
