package ru.devirta.backvr.exception;

public class VariantOfAnswerNotFoundException extends RuntimeException {
    public VariantOfAnswerNotFoundException(String message) {
        super(message);
    }
}
