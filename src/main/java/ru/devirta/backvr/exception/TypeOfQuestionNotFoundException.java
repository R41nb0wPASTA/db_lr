package ru.devirta.backvr.exception;

public class TypeOfQuestionNotFoundException extends RuntimeException {
    public TypeOfQuestionNotFoundException(String message) {
        super(message);
    }
}
