package ru.devirta.backvr.exception;

public class UserHasAccessToFormNotFoundException extends RuntimeException {
    public UserHasAccessToFormNotFoundException(String message) {
        super(message);
    }
}
