package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.UserDao;
import ru.devirta.backvr.dto.RoleDto;
import ru.devirta.backvr.model.Role;
import ru.devirta.backvr.model.User;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    RoleService roleService;

    private final UserDao userDao;

    @Autowired
    public UserDetailsServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userDao.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Invalid email"));
        /*List<Role> roles = Collections.emptyList();
        Long id = user.getRoleId();
        RoleDto roleDto = roleService.getRoleById(id);
        Role u_role = new Role(roleDto);
        roles.add(u_role);*/

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());//mapRolesToAuthorities(roles));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
    }

}
