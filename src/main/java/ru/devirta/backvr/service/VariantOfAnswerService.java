package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.VariantOfAnswerDao;
import ru.devirta.backvr.dto.VariantOfAnswerDto;
import ru.devirta.backvr.exception.VariantOfAnswerNotFoundException;
import ru.devirta.backvr.model.VariantOfAnswer;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VariantOfAnswerService {
    private VariantOfAnswerDao variantOfAnswerDao;

    @Autowired
    public VariantOfAnswerService(VariantOfAnswerDao variantOfAnswerDao) {
        this.variantOfAnswerDao = variantOfAnswerDao;
    }

    public VariantOfAnswerDto getVariantOfAnswerById(Long id){
        return new VariantOfAnswerDto(variantOfAnswerDao.findById(id).orElseThrow(()->
                new VariantOfAnswerNotFoundException("Variant of answer with id " + id + "not found.")));
    }

    public VariantOfAnswerDto saveVariantOfAnswer(VariantOfAnswerDto variantOfAnswerDto){
        VariantOfAnswer variantOfAnswer = new VariantOfAnswer(variantOfAnswerDto);
        return new VariantOfAnswerDto(variantOfAnswerDao.save(variantOfAnswer));
    }

    public void delete(Long id) {
        VariantOfAnswer variantOfAnswer = variantOfAnswerDao.findById(id).orElseThrow(() -> new VariantOfAnswerNotFoundException("Variant of answer with " + id + " not found"));
        variantOfAnswerDao.delete(variantOfAnswer);
    }

}
