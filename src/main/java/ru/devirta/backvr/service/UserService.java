package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.UserDao;
import ru.devirta.backvr.dto.UserDto;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.User;

@Service
public class UserService {
    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao userDao, PasswordEncoder passwordEncoder){
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto getUserByEmail(String email){
        return new UserDto(userDao.findByEmail(email).orElseThrow(()->
                new UserNotFoundException("User with email " + email + "not found.")));
    }

    public UserDto getUserById(Long id){
        return new UserDto(userDao.findById(id).orElseThrow(()->
                new UserNotFoundException("User with id " + id + "not found.")));
    }

    public UserDto saveUser(UserDto userDto){
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = new User(userDto);
        return new UserDto(userDao.save(user));
    }

    public UserDto getAuthUserCredentials() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getUserByEmail(userDetails.getUsername());
    }

    public void delete(Long id) {
        User user = userDao.findById(id).orElseThrow(() -> new UserNotFoundException("User with " + id + " not found"));
        userDao.delete(user);
    }
}
