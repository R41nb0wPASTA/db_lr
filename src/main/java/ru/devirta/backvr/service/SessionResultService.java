package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import ru.devirta.backvr.dao.QuestionDao;
import ru.devirta.backvr.dao.SessionResultDao;
import ru.devirta.backvr.dao.VariantOfAnswerDao;
import ru.devirta.backvr.dto.QuestionDto;
import ru.devirta.backvr.dto.SessionResultDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.exception.SessionResultNotFoundException;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.model.VariantOfAnswer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SessionResultService {
    private final SessionResultDao sessionResultDao;
    private final VariantOfAnswerDao variantOfAnswerDao;
    private final QuestionDao questionDao;

    @Autowired
    public SessionResultService(SessionResultDao sessionResultDao, VariantOfAnswerDao variantOfAnswerDao, QuestionDao questionDao) {
        this.sessionResultDao = sessionResultDao;
        this.variantOfAnswerDao = variantOfAnswerDao;
        this.questionDao = questionDao;
    }

    public SessionResultDto getSessionResultById(Long id){
        return new SessionResultDto(sessionResultDao.findById(id).orElseThrow(()->
                new SessionResultNotFoundException("Session result with id " + id + "not found.")));
    }

    public List<SessionResult> getSessionResultByQuestionId(Long id){
        List<VariantOfAnswer> variantOfAnswers = variantOfAnswerDao.findAllByQuestion_id(id);
        List<Long> sessionResultsIds = new ArrayList<>();
        List<SessionResult> sessionResults = new ArrayList<>();

        variantOfAnswers.forEach(s -> {sessionResultsIds.add(s.getId());});
        sessionResultsIds.forEach(s -> {sessionResults.addAll(sessionResultDao.findAllByVariantOfAnswerId(s));});

        return sessionResults;
    }

    public List<SessionResult> getSessionResultByFormId(Long id){
        List<Question> questions = questionDao.findAllByForm_id(id);
        List<Long> questionsIds = new ArrayList<>();
        List<VariantOfAnswer> variantOfAnswers = new ArrayList<>();
        List<Long> sessionResultsIds = new ArrayList<>();
        List<SessionResult> sessionResults = new ArrayList<>();

        questions.forEach(s -> {questionsIds.add(s.getId());});
        questionsIds.forEach(q_id -> {variantOfAnswers.addAll(variantOfAnswerDao.findAllByQuestion_id(q_id));});
        variantOfAnswers.forEach(s -> {sessionResultsIds.add(s.getId());});
        System.out.println(sessionResultsIds);
        sessionResultsIds.forEach(s -> {sessionResults.addAll(sessionResultDao.findAllByVariantOfAnswerId(s));});

        return sessionResults;
    }

    public SessionResultDto saveSessionResult(SessionResultDto sessionResultDto){
        SessionResult sessionResult = new SessionResult(sessionResultDto);
        return new SessionResultDto(sessionResultDao.save(sessionResult));
    }

    public void delete(Long id) {
        SessionResult sessionResult = sessionResultDao.findById(id).orElseThrow(() ->
                new SessionResultNotFoundException("Session result result with " + id + " not found"));
        sessionResultDao.delete(sessionResult);
    }
}