package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.TypeOfQuestionDao;
import ru.devirta.backvr.dto.TypeOfQuestionDto;
import ru.devirta.backvr.exception.TypeOfQuestionNotFoundException;
import ru.devirta.backvr.model.TypeOfQuestion;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeOfQuestionService {
    private TypeOfQuestionDao typeOfQuestionDao;

    @Autowired
    public TypeOfQuestionService(TypeOfQuestionDao typeOfQuestionDao) {
        this.typeOfQuestionDao = typeOfQuestionDao;
    }

    public TypeOfQuestionDto getTypeOfQuestionById(Long id){
        return new TypeOfQuestionDto(typeOfQuestionDao.findById(id).orElseThrow(()->
                new TypeOfQuestionNotFoundException("Type of question with id " + id + "not found.")));
    }

    public TypeOfQuestionDto getTypeOfQuestionByName(String name){
        return new TypeOfQuestionDto(typeOfQuestionDao.findByName(name).orElseThrow(()->
                new TypeOfQuestionNotFoundException("Type of question with name " + name + "not found.")));
    }

    public TypeOfQuestionDto saveTypeOfQuestion(TypeOfQuestionDto typeOfQuestionDto){
        TypeOfQuestion typeOfQuestion = new TypeOfQuestion(typeOfQuestionDto);
        return new TypeOfQuestionDto(typeOfQuestionDao.save(typeOfQuestion));
    }

    public void delete(Long id) {
        TypeOfQuestion typeOfQuestion = typeOfQuestionDao.findById(id).orElseThrow(() -> new TypeOfQuestionNotFoundException("Type of question with " + id + " not found"));
        typeOfQuestionDao.delete(typeOfQuestion);
    }

}
