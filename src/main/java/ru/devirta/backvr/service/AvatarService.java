package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.devirta.backvr.dao.RoleDao;
import ru.devirta.backvr.dto.RoleDto;
import ru.devirta.backvr.exception.RoleNotFoundException;
import ru.devirta.backvr.model.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.AvatarDao;
import ru.devirta.backvr.dto.AvatarDto;
import ru.devirta.backvr.exception.AvatarNotFoundException;
import ru.devirta.backvr.model.Avatar;

@Service
public class AvatarService {
    private final AvatarDao avatarDao;

    @Autowired
    public AvatarService(AvatarDao avatarDao) {
        this.avatarDao = avatarDao;
    }

    public AvatarDto getAvatarById(Long id){
        return new AvatarDto(avatarDao.findById(id).orElseThrow(()->
                new AvatarNotFoundException("Avatar with id " + id + "not found.")));
    }

    public AvatarDto saveAvatar(AvatarDto avatarDto){
        Avatar avatar = new Avatar(avatarDto);
        return new AvatarDto(avatarDao.save(avatar));
    }

    public void delete(Long id) {
        Avatar avatar = avatarDao.findById(id).orElseThrow(() -> new RoleNotFoundException("Role with " + id + " not found"));
        avatarDao.delete(avatar);
    }
}
