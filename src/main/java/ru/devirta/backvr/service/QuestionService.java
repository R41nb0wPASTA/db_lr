package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.QuestionDao;
import ru.devirta.backvr.dto.QuestionDto;
import ru.devirta.backvr.exception.QuestionNotFoundException;
import ru.devirta.backvr.model.Question;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {
    private QuestionDao questionDao;

    @Autowired
    public QuestionService(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    public QuestionDto getQuestionById(Long id){
        return new QuestionDto(questionDao.findById(id).orElseThrow(()->
                new QuestionNotFoundException("Question with id " + id + "not found.")));
    }

    public QuestionDto saveQuestion(QuestionDto questionDto){
        Question question = new Question(questionDto);
        return new QuestionDto(questionDao.save(question));
    }

    public void delete(Long id) {
        Question question = questionDao.findById(id).orElseThrow(() -> new QuestionNotFoundException("Question with " + id + " not found"));
        questionDao.delete(question);
    }

}
