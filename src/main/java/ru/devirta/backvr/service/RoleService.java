package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.dao.RoleDao;
import ru.devirta.backvr.dto.RoleDto;
import ru.devirta.backvr.exception.RoleNotFoundException;
import ru.devirta.backvr.model.Role;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {
    private RoleDao roleDao;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public RoleDto getRoleById(Long id){
        return new RoleDto(roleDao.findById(id).orElseThrow(()->
                new RoleNotFoundException("Role with id " + id + "not found.")));
    }

    public RoleDto getRoleByName(String name){
        return new RoleDto(roleDao.findByRoleName(name).orElseThrow(()->
                new RoleNotFoundException("Role with name " + name + "not found.")));
    }

    public RoleDto saveRole(RoleDto roleDto){
        Role role = new Role(roleDto);
        return new RoleDto(roleDao.save(role));
    }

    public void delete(Long id) {
        Role role = roleDao.findById(id).orElseThrow(() -> new RoleNotFoundException("Role with " + id + " not found"));
        roleDao.delete(role);
    }

}
