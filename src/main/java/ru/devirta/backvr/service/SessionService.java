package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.devirta.backvr.dao.SessionDao;
import ru.devirta.backvr.dto.SessionDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.exception.SessionNotFoundException;
import ru.devirta.backvr.model.Session;

@Service
public class SessionService {
    private final SessionDao sessionDao;

    @Autowired
    public SessionService(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public SessionDto getSessionById(Long id){
        return new SessionDto(sessionDao.findById(id).orElseThrow(()->
                new SessionNotFoundException("Session with id " + id + "not found.")));
    }

    public SessionDto saveSession(SessionDto sessionDto){
        Session session = new Session(sessionDto);
        return new SessionDto(sessionDao.save(session));
    }

    public void delete(Long id) {
        Session session = sessionDao.findById(id).orElseThrow(() ->
                new SessionNotFoundException("Session with " + id + " not found"));
        sessionDao.delete(session);
    }
}
