package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.devirta.backvr.dao.FormDao;
import ru.devirta.backvr.dao.QuestionDao;
import ru.devirta.backvr.dao.VariantOfAnswerDao;
import ru.devirta.backvr.dto.FormDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.exception.FormNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.model.VariantOfAnswer;

import javax.swing.text.html.parser.Entity;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FormService {
    private final FormDao formDao;
    private final QuestionDao questionDao;
    private final VariantOfAnswerDao variantOfAnswerDao;

    @Autowired
    public FormService(FormDao formDao, QuestionDao questionDao,VariantOfAnswerDao variantOfAnswerDao) {
        this.formDao = formDao;
        this.questionDao = questionDao;
        this.variantOfAnswerDao = variantOfAnswerDao;
    }

    public FormDto getFormById(Long id){
        return new FormDto(formDao.findById(id).orElseThrow(()->
                new FormNotFoundException("Form with id " + id + " not found.")));
    }

    public List<Form> getAllFormsByUserId(Long id){
        List<Form> forms = formDao.findAllByUser_id(id)
                .orElseThrow(() -> new UserNotFoundException("User with id " + id + " not found."));

        return  forms;
    }

    public List<Question> getFormQuestionsWithVarsById(Long id){
        List<Question> questions = questionDao.findAllByForm_id(id);
        List<Long> questionsIds = new ArrayList<>();
        List<VariantOfAnswer> variantOfAnswers = new ArrayList<>();

        List <Object> retur = new ArrayList();
        int i = 0;
        questions.forEach(s -> {retur.add(s); retur.addAll(variantOfAnswerDao.findAllByQuestion_id(s.getId()));});
        System.out.println(retur);
        //questionsIds.forEach(q_id -> {variantOfAnswers.addAll(variantOfAnswerDao.findAllByQuestion_id(q_id));});



        return questions;
    }

    public List<VariantOfAnswer> getAllVarsByQuestionId(Long id){
        List<VariantOfAnswer> variantOfAnswers = variantOfAnswerDao.findAllByQuestion_id(id);

        return variantOfAnswers;
    }

    public Form getMostPopularForm(){
        List<Form> forms = formDao.findAll();

        Map<Form, Integer> map = new HashMap<>();

        for (Form t : forms) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<Form, Integer> max = null;

        for (Map.Entry<Form, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue())
                max = e;
        }

        return max.getKey();
    }

    public FormDto saveForm(FormDto formDto){
        Form form = new Form(formDto);
        return new FormDto(formDao.save(form));
    }

    public void delete(Long id) {
        Form form = formDao.findById(id).orElseThrow(() ->
                new FormNotFoundException("Form with " + id + " not found"));
        formDao.delete(form);
    }
}

