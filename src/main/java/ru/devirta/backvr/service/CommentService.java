package ru.devirta.backvr.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.devirta.backvr.dao.CommentDao;
import ru.devirta.backvr.dto.CommentDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.devirta.backvr.exception.CommentNotFoundException;
import ru.devirta.backvr.exception.UserNotFoundException;
import ru.devirta.backvr.model.Comment;
import ru.devirta.backvr.model.Form;

import java.util.List;

@Service
public class CommentService {
    private final CommentDao commentDao;

    @Autowired
    public CommentService(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public CommentDto getCommentById(Long id){
        return new CommentDto(commentDao.findById(id).orElseThrow(()->
                new CommentNotFoundException("Comment with id " + id + "not found.")));
    }

    public List<Comment> getAllCommentsByUserId(Long id){
        List<Comment> comments = commentDao.findAllByUser_id(id)
                .orElseThrow(() -> new UserNotFoundException("User with id " + id + " not found."));

        return  comments;
    }

    public CommentDto saveComment(CommentDto commentDto){
        Comment comment = new Comment(commentDto);
        return new CommentDto(commentDao.save(comment));
    }

    public void delete(Long id) {
        Comment comment = commentDao.findById(id).orElseThrow(() ->
                new CommentNotFoundException("Comment with " + id + " not found"));
        commentDao.delete(comment);
    }
}
