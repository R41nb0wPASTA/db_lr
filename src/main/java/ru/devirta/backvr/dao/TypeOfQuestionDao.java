package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.TypeOfQuestion;

import java.util.Optional;

@Repository
public interface TypeOfQuestionDao extends CrudRepository<TypeOfQuestion, Long> {
    Optional<TypeOfQuestion> findById(Long id);
    Optional<TypeOfQuestion> findByName(String name);
}
