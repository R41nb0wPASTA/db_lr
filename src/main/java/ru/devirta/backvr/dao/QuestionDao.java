package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Question;
import ru.devirta.backvr.model.VariantOfAnswer;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionDao extends CrudRepository<Question, Long> {
    List<Question> findAllByForm_id(Long id);
    Optional<Question> findById(Long id);
}
