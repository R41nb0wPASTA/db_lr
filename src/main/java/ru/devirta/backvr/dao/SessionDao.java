package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Session;

import java.util.Optional;

@Repository
public interface SessionDao extends CrudRepository<Session, Long> {
    Optional<Session> findById(Long id);
}
