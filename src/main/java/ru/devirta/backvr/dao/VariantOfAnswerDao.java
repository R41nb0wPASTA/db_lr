package ru.devirta.backvr.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.model.VariantOfAnswer;


import java.util.List;
import java.util.Optional;

import static org.hibernate.loader.Loader.SELECT;

@Repository
public interface VariantOfAnswerDao extends CrudRepository<VariantOfAnswer, Long> {
    VariantOfAnswer getByQuestion_id(Long id);
    List<VariantOfAnswer> findAllByQuestion_id(Long id);
    Optional<VariantOfAnswer> findById(Long id);
}
