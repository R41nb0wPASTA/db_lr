package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Form;
import ru.devirta.backvr.model.SessionResult;
import ru.devirta.backvr.model.VariantOfAnswer;

import java.util.List;
import java.util.Optional;

@Repository
public interface SessionResultDao extends CrudRepository<SessionResult, Long> {
    SessionResult getByVariantOfAnswerId(Long id);
    List<SessionResult> findAllByVariantOfAnswerId(Long id);
    Optional<SessionResult> findById(Long id);
}

