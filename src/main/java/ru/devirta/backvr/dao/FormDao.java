package ru.devirta.backvr.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Form;

import java.util.List;
import java.util.Optional;

@Repository
public interface FormDao extends CrudRepository<Form, Long> {
    List<Form> findAll();

    Optional<Form> findById(Long id);
    Optional<List<Form>> findAllByUser_id(Long id);
}
