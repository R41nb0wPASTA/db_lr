package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Comment;
import ru.devirta.backvr.model.Form;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentDao extends CrudRepository<Comment, Long> {
    Optional<Comment> findById(Long id);
    Optional<List<Comment>> findAllByUser_id(Long id);
}
