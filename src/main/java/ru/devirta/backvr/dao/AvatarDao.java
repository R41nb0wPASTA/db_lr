package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Avatar;

import java.util.Optional;

@Repository
public interface AvatarDao extends CrudRepository<Avatar, Long> {
    Optional<Avatar> findById(Long id);
}
