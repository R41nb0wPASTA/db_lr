package ru.devirta.backvr.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.devirta.backvr.model.Role;

import java.util.Optional;

@Repository
public interface RoleDao extends CrudRepository<Role, Long> {
    Optional<Role> findById(Long id);
    Optional<Role> findByRoleName(String name);
}
